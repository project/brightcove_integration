<?php
/**
 * @file
 * Video menu common functions.
 */

/**
 * Redirect user to proper video.
 *
 * @param StdClass $node
 *   Video object
 */
function brightcove_integration_play_video($node) {
  $brightcove_id = _brightcove_integration_get_brightcode_id_field($node, 'field_brightcove_id');

  if ($brightcove_id) {
    // Cronstruct the URL to be handled and get he Json.
    $brightcove_request_url = _brightcove_integration_get_brightcove_request_url($brightcove_id);

    // Treat the Json from the request.
    _brightcove_integration_handle_video_request($brightcove_request_url);
  }
  else {
    drupal_goto(variable_get('brightcove_integration_error_page_path', ''));
  }
}

/**
 * Return a safe value for a node field.
 *
 * Return is already escaped and in correct language.
 *
 * @param StdClass $node
 *   Node Object
 * @param string $field_name
 *   String with the node's field name.
 *
 * @return String
 *   Rendered field.
 */
function _brightcove_integration_get_brightcode_id_field($node, $field_name) {
  $field       = field_get_items('node', $node, $field_name);
  $field_value = field_view_value('node', $node, $field_name, $field[0]);

  return render($field_value);
}

/**
 * Return brightcove full video url.
 *
 * @param string $brightcove_id
 *   Video id on Brightcove
 *
 * @return String
 *   Video url.
 */
function _brightcove_integration_get_brightcove_request_url($brightcove_id) {
  $brightcove_token = variable_get('brightcove_integration_brightcove_token', '');
  $url              = variable_get('brightcove_integration_brightcove_url', BRIGHTCOVE_INTEGRATION_BASE_URL_SETTINGS);

  $patterns = array(
    '%brightcove_token%',
    '%brightcove_id%',
  );

  $replaces = array(
    $brightcove_token,
    $brightcove_id,
  );

  return str_replace($patterns, $replaces, $url);
}

/**
 * Perform request to Brightcove API and handle response.
 *
 * Redirect to video full url on success, or to error page.
 *
 * @param string $url
 *   Request url
 */
function _brightcove_integration_handle_video_request($url) {
  $result = drupal_http_request($url);

  if ($result->code == '200' && !empty($result->data)) {
    $video_obj = json_decode($result->data);

    if (!isset($video_obj->error) && $video_obj->videoFullLength->url) {
      drupal_goto($video_obj->videoFullLength->url, array('absolute' => TRUE));
    }
    else {
      drupal_goto(variable_get('brightcove_integration_error_page_path', ''));
    }
  }
}

/**
 * Get information about Brightcove video node.
 */
function _brightcove_integration_get_nodes_info() {
  $nids = db_select('node', 'n')
    ->fields('n', array('nid'))
    ->fields('n', array('type'))
    ->condition('n.type', 'brightcove_integration_type')
    ->execute()
    ->fetchCol();

  $brightcove_nodes      = node_load_multiple($nids);
  $brightove_block_nodes = array();

  foreach ($brightcove_nodes as $nid => $node_info) {
    if (isset($node_info->field_brightcove_block)) {
      $field_brightcove_block = field_get_items('node', $node_info, 'field_brightcove_block');
      if ($field_brightcove_block[0]['value'] == 1) {
        $brightove_block_nodes[$nid] = $node_info;
      }
    }
  }

  return $brightove_block_nodes;
}

/**
 * Inserts new video player settings entry in database.
 *
 * @param array $values
 *   An array containing the format 'database_column' => 'value'.
 */
function _brightcove_integration_insert_new_player_settings($values) {
  db_insert('brightcove_integration_player')
  ->fields($values)
  ->execute();
}

/**
 * Updates a given video player settings in database.
 *
 * @param array $values
 *   An array containing the format 'database_column' => 'value'.
 */
function _brightcove_integration_update_player_settings($values) {
  db_update('brightcove_integration_player')
  ->condition('nid', $values['nid'], '=')
  ->fields($values)
  ->execute();
}

/**
 * Returns the video player settings by a given node ID.
 *
 * @param int $nid
 *   The ID of the related node.
 */
function _brightcove_integration_get_player_settings_by_nid($nid) {
  $settings = db_select('brightcove_integration_player', 'b')
  ->fields('b')
  ->condition('nid', $nid, '=')
  ->execute()
  ->fetchAssoc();

  return $settings;
}

/**
 * Saves a video player settings.
 *
 * @param array $values
 *   An array containing the format 'database_column' => 'value'.
 */
function _brightcove_integration_save_player_settings($values) {
  if (isset($values['nid']) && $values['nid']) {
    $current_settings = _brightcove_integration_get_player_settings_by_nid($values['nid']);
    if ($current_settings) {
      _brightcove_integration_update_player_settings($values);
    }
    else {
      _brightcove_integration_insert_new_player_settings($values);
    }
  }
}

/**
 * Validates whether the value of an element is numerical.
 */
function _brightcove_integration_element_validate_is_numeric($element, &$form_state, $form) {
  if (!form_get_errors() && (!is_numeric($element['#value']))) {
    form_error($element, t('"@name" field should only receive a numeric value.', array('@name' => $element['#title'])));
  }
}

/**
 * Validates whether the value of an element is Hex color string format.
 */
function _brightcove_integration_element_validate_is_hex_color($element, &$form_state, $form) {
  // Check for a hex color string '#c1c2b4'.
  if (!form_get_errors() && (!preg_match('/^#[a-f0-9]{6}$/i', $element['#value']))) {
    form_error($element, t('"@name" field should only receive a hex color string. Example: #FFFFFF', array('@name' => $element['#title'])));
  }
}

/**
 * Render a block related to a given node ID of type brightcove integration.
 *
 * @param int $nid
 *   The node ID.
 */
function _brightcove_integration_render_block_by_nid($nid) {
  $brightcove_integration_api_js         = variable_get('brightcove_integration_api_js_url', '');
  $brightcove_integration_experiences_js = variable_get('brightcove_integration_experiences_js_url', '');

  drupal_add_js($brightcove_integration_api_js, 'external');

  if (!empty($brightcove_integration_experiences_js)) {
    drupal_add_js($brightcove_integration_experiences_js, 'external');
  }

  $player_settings = _brightcove_integration_get_block_configuration($nid);
  return theme('brightcove_integration', array('player_settings' => $player_settings));
}

/**
 * Function to get the block video configuration.
 *
 * @param int $nid
 *   Node ID
 *
 * @return array
 *   Array with video configuration
 */
function _brightcove_integration_get_block_configuration($nid) {
  $brightcove_integration_video_node = node_load($nid);
  $video_id   = field_get_items('node', $brightcove_integration_video_node, 'field_brightcove_id');
  $player_settings = _brightcove_integration_get_player_settings_by_nid($nid);

  return array(
    'bid'       => isset($player_settings['bid']) ? $player_settings['bid'] : '',
    'bgcolor'   => isset($player_settings['player_bgcolor']) ? $player_settings['player_bgcolor'] : '#FFFFFF',
    'width'     => isset($player_settings['player_width']) ? $player_settings['player_width'] : '250',
    'height'    => isset($player_settings['player_height']) ? $player_settings['player_height'] : '250',
    'playerid'  => isset($player_settings['player_id']) ? $player_settings['player_id'] : '',
    'playerkey' => isset($player_settings['player_key']) ? $player_settings['player_key'] : '',
    'token'     => variable_get('brightcove_integration_brightcove_token', ''),
    'videoid'   => $video_id[0]['value'],
  );
}

/**
 * Deletes a player settings database entry.
 *
 * @param int $nid
 *   The node id.
 */
function _brightcove_integration_delete_player_settings($nid) {
  db_delete('brightcove_integration_player')
  ->condition('nid', $nid)
  ->execute();
}

/**
 * Deletes a Drupal block created by the module.
 *
 * @param string $block_delta
 *   Block delta.
 */
function _brightcove_integration_delete_block($block_delta) {
  db_delete('block')
  ->condition('delta', $block_delta)
  ->execute();
}
