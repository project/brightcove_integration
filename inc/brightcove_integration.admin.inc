<?php
/**
 * @file
 * Admin forms for configurations
 */

/**
 * Implements function to show admin configuration form().
 */
function brightcove_integration_admin_general_settings() {
  $form = array();

  $form['brightcove_integration'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('General settings'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );

  $form['brightcove_integration']['brightcove_integration_brightcove_token'] = array(
    '#type'          => 'textfield',
    '#size'          => 156,
    '#title'         => t('BrightCove token'),
    '#description'   => t('Configure brightcove token to read video URLs.'),
    '#default_value' => variable_get('brightcove_integration_brightcove_token', ''),
    '#required'      => TRUE,
  );

  $form['brightcove_integration']['brightcove_integration_brightcove_url'] = array(
    '#type'          => 'textfield',
    '#size'          => 156,
    '#maxlength'     => 256,
    '#title'         => t('Brightcove URL'),
    '#description'   => t('Insert the base Brightcove URL to render the video. Use these token parameters: %brightcove_id% %brightcove_token%'),
    '#default_value' => variable_get('brightcove_integration_brightcove_url', BRIGHTCOVE_INTEGRATION_BASE_URL_SETTINGS),
    '#required'      => TRUE,
  );

  $form['brightcove_integration']['brightcove_integration_error_page_path'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Error page path'),
    '#description'   => t('User will be redirect to this path when some error occurs with brightcove.'),
    '#default_value' => variable_get('brightcove_integration_error_page_path', ''),
    '#required'      => FALSE,
  );

  $form['brightcove_integration_js'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('JS settings'),
    '#collapsible' => TRUE,
    '#collapsed'   => FALSE,
  );

  $form['brightcove_integration_js']['brightcove_integration_api_js_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Javascript API url'),
    '#default_value' => variable_get('brightcove_integration_api_js_url', ''),
    '#required'      => FALSE,
  );

  $form['brightcove_integration_js']['brightcove_integration_experiences_js_url'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Experiences JS url'),
    '#default_value' => variable_get('brightcove_integration_experiences_js_url', ''),
    '#required'      => FALSE,
  );

  return system_settings_form($form);
}
