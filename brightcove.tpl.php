<?php
/**
 * @file
 * Template to display the basic video player object.
 *
 * Variable available: $player_settings
 */
?>

<object class="BrightcoveExperience" id="experience<?php print $player_settings['bid'];?>">
  <param name="wmode" value="transparent" />
  <param name="bgcolor" value="<?php print $player_settings['bgcolor']; ?>" />
  <param name="width" value="<?php print $player_settings['width']; ?>" />
  <param name="height" value="<?php print $player_settings['height']; ?>" />
  <param name="playerID" value="<?php print $player_settings['playerid']; ?>" />
  <param name="playerKey" value="<?php print $player_settings['playerkey']; ?>" />
  <param name="isVid" value="true" />
  <param name="isUI" value="true" />
  <param name="dynamicStreaming" value="true" />
  <param name="htmlFallback" value="true" />
  <param name="includeAPI" value="true" />
  <param name="@videoPlayer" value="<?php print $player_settings['videoid']; ?>" />
  <param name="templateLoadHandler" value="BCLS.onTemplateLoad" />
  <param name="templateReadyHandler" value="BCLS.onTemplateReady" />
</object>
