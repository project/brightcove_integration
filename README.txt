CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Terminology
 * Instalation
 * Credits
 
INTRODUCTION
------------
This module provides integration with Brigthcove Videos.

http://www.brightcove.com

TERMINOLOGY
------------
For now, the module provides two ways to instantiate videos:

* Providing the final url of the video

  For performance reasons, direct access to the urls of the videos hosted on
  Brigthcove are dynamic, so the module handling the API provides the URL of
  the video in a simple way. Having the final URL of the video is useful when
  you want to link something direct to access the video, often used for mobile
  devices when you want the video start using native player device.
  Use the path brightcove/video/%nid% to get the video. 

* Providing a embedded Video Player

  The module provides a Drupal Block to each video configured. This block
  contains the embedded video player, this way you can position the player
  in the pages that you want.

INSTALATION
-----------
Install the brightcove_integration module (standard Drupal way).
Go to admin/config/media/brightcove-integration and configure the module.
Go to node/add/brightcove-integration-type to create a new video content.